#----EXERCISE 1/132

# first = int(input("Podaj liczbę początkową: "))
# last = int(input("Podaj liczbę końcową: "))
# step = int(input("Podaj różnicę między liczbami: "))

# result = range(first, last, step) 
# for i in result:
#   print(i)

#----EXERCISE 2/132

# message = str(input("Napisz wiadomość: "))
# palindrome = ""

# for e in message:
#     palindrome = e + palindrome 
# print("Palindrom wiadomości:", palindrome)

#----EXERCISE 3/132
import random

words_list = ["kot", "pies", "placki", "czekolada", "jesień"]

choice = random.choice(words_list)

print("ODGADNIJ SŁOWO!")
print("Liczba liter w wylosowanym słowie: " + str(len(choice)))

print("Masz 5 szans by sprawdzić, czy wylosowane słowo zawiera Twoją literę.")

for e in range(5):
    letter = input("Podaj literę: ")
    if letter in choice:
        print("Tak!")
    else:
        print("Nie!")

result = input("Spróbuj odgadnąć słowo: ")

if result == choice:
    print("Udało się! Jest to: " + str(result) + ". Gratulacje!")
else:
    print("Niestety, to nie to słowo! :(")

