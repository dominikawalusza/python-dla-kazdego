# 1. Utwórz program, który wypisuje listę słów w przypadkowej kolejności. Program powinien wypisać wszystkie słowa bez żadnych powtórzeń.

import random

words_list = ["Ala", "ma", "kota", "a", "kot", "Alę"]
random.shuffle(words_list)
print(words_list)